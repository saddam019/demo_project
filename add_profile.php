<?php
session_start();
if ($_SESSION['user'] == "") {
    header("Location: index.php");
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amar prosno Online</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">


    <script src="lib/jquery.js"></script>
    <script src="dist/jquery.validate.js"></script>
    <script>
        $.validator.setDefaults({
            submitHandler: function() {
                alert("submitted!");
            }
        });

        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    fname: "required",
                    lname: "required",
                    mobile_no: {
                        required: true,
                        minlength: 11
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    dob: {
                        required: true,
                        minlength: 5
                    },
                    confirm_password: {
                        required: true,
                        minlength: 5,
                        equalTo: "#password"
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    topic: {
                        required: "#newsletter:checked",
                        minlength: 2
                    },
                    agree: "required"
                },
                messages: {
                    firstname: "Please enter your firstname",
                    lastname: "Please enter your lastname",
                    username: {
                        required: "Please enter a username",
                        minlength: "Your username must consist of at least 2 characters"
                    },
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    confirm_password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long",
                        equalTo: "Please enter the same password as above"
                    },
                    email: "Please enter a valid email address",
                    agree: "Please accept our policy",
                    topic: "Please select at least 2 topics"
                }
            });

            // propose username by combining first- and lastname
            $("#username").focus(function() {
                var firstname = $("#firstname").val();
                var lastname = $("#lastname").val();
                if (firstname && lastname && !this.value) {
                    this.value = firstname + "." + lastname;
                }
            });

            //code to hide topic selection, disable for demo
            var newsletter = $("#newsletter");
            // newsletter topics are optional, hide at first
            var inital = newsletter.is(":checked");
            var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
            var topicInputs = topics.find("input").attr("disabled", !inital);
            // show when newsletter is checked
            newsletter.click(function() {
                topics[this.checked ? "removeClass" : "addClass"]("gray");
                topicInputs.attr("disabled", !this.checked);
            });
        });
    </script>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<style type="text/css">


    /* Sidebar navigation */
    .nav-sidebar {
        margin-right: -21px; /* 20px padding + 1px border */
        margin-bottom: 20px;
        margin-left: -20px;
    }

    .nav-sidebar > li > a {
        padding-right: 20px;
        padding-left: 20px;
    }

    .nav-sidebar > .active > a,
    .nav-sidebar > .active > a:hover,
    .nav-sidebar > .active > a:focus {
        color: #fff;
        background-color: #428bca;
    }


</style>

<body>


<title>Amarprosno online</title>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<div class="header_area">
    <div class="container header">
        <div class="row">
            <div class="col-md-12">
                <div class="header_nav">

                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar" aria-expanded="false" aria-controls="navbar">

                                </button>
                                <a class="navbar-brand logo" style="color: #15A4FA;font-size: 30px;" ;
                                   href="home.php">Amarprosno.com</a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="home.php">Questions</a></li>
                                    <li><a href="ask.php">Ask Question</a></li>
                                    <li><a href="index.php">Index</a></li>
                                    <li>
                                        <form action="search.php" method="post">
                                            <div class="form-group">
                                                <input type="search" name="search" tabindex="1"
                                                       class="form-control" placeholder="Search Question">
                                            </div>
                                    </li>
                                    <li>
                                        <div class="form-group">
                                            <input type="submit" name="btn" class="form-control" placeholder="Search"
                                                   value="Search">
                                        </div>
                                    </li>
                                    </form>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">

                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-haspopup="true" aria-expanded="false">
                                            <span class="glyphicon glyphicon-user"></span>&nbsp;Hi <?php echo $_SESSION['user'] ?>
                                            &nbsp;<span
                                                class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="Views/Auth/logout.php"><span></span>Sign Out</a>

                                            <li><a href="profile.php"><span></span>Profile</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <?php

                include "vendor/autoload.php";
                use App\Auth\Auth;

                $question = new Auth();
                $view_num_qst = $question->countrowforqst();
                $obj = new Auth();
                $num_of_answer = $obj->countrowforanswer();
                ?>
                <li class="active"><a href="">Overview <span class="sr-only">(current)</span></a></li>
                <li><a href="questions.php?user_id=<?php echo $_SESSION['user_id'] ?>">My Questions <span
                            class="badge"><?php echo $view_num_qst ?></span></a></li>
                <li><a href="my_answers.php">My Answers <span class="badge"><?php echo $num_of_answer ?></span></a></li>
                <li><a href="">Export</a></li>
            </ul>

        </div>
        <?php
        if (!isset($_GET['user_id'])){
        ?>

    </div>
</div>


<?php } ?>

<?php ?>


<div class="user_profile">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h1>Dashboard</h1> <div class="edit_my_profile">
                    <form action="Views/Auth/add_profile.php" id="signupForm" method="post">
                        <div class="form-group">
                            <label for="title">First Name</label>
                            <input type="text" class="form-control" name="fname" placeholder="First Name">
                        </div>


                        <div class="form-group">
                            <label for="title">Last Name</label>
                            <input type="text" class="form-control" name="lname" placeholder="Last Name">
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Mobile No</label>
                            <input type="number" class="form-control" name="mobile_no"
                                   placeholder="Mobile No">
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="gender" id="optionsRadios1" value="male" checked>
                                Male
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="gender" id="optionsRadios1" value="female">
                                Female
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Date of Birth</label>
                            <input type="date" class="form-control" name="dob" placeholder="Date of birth">
                        </div>

                        <div class="form-group">
                            <label>Address</label>
                            <textarea class="form-control" rows="5" name="address" id="comment"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Experience</label>
                            <input type="text" class="form-control" name="experience"
                                   placeholder="Experience">
                        </div>

                        <div class="form-group">
                            <input type="submit" name="register-submit" id="register-submit"
                                   tabindex="4" class="form-control btn btn-register"
                                   value="Submit">
                        </div>
                    </form>
                </div>
            </div>

            </div>
        </div>
    </div>



<?php if (isset($_GET['user_id'])) { ?>
    <div class="edit_profile_area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="edit_profile">

                        <h3>Edit Profile</h3>

                        <div class="col-md-12">
                            <div class="edit_my_profile">
                                <form action="Views/Auth/add_profile.php" method="post">
                                    <div class="form-group">
                                        <label for="title">First Name</label>
                                        <input type="text" class="form-control" name="fname" placeholder="First Name">
                                    </div>


                                    <div class="form-group">
                                        <label for="title">Last Name</label>
                                        <input type="text" class="form-control" name="lname" placeholder="Last Name">
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Mobile No</label>
                                        <input type="number" class="form-control" name="mobile_no"
                                               placeholder="Mobile No">
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="optionsRadios1" value="male" checked>
                                            Male
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" id="optionsRadios1" value="female">
                                            Female
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date of Birth</label>
                                        <input type="date" class="form-control" name="dob" placeholder="Date of birth">
                                    </div>

                                    <div class="form-group">
                                        <label>Address</label>
                                        <textarea class="form-control" rows="5" name="address" id="comment"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Experience</label>
                                        <input type="text" class="form-control" name="experience"
                                               placeholder="Experience">
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" name="register-submit" id="register-submit"
                                               tabindex="4" class="form-control btn btn-register"
                                               value="Submit">
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
}
?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


</body>
</html>