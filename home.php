<?php
session_start();
if($_SESSION['user']=="")
{
    header("Location: index.php");
    echo "something changed";
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amar prosno Online</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="header_area">
    <div class="container header">
        <div class="row">
            <div class="col-md-12">
                <div class="header_nav">

                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar" aria-expanded="false" aria-controls="navbar">

                                </button>
                                <a class="navbar-brand logo" style="color: #15A4FA;font-size: 30px;" ;
                                   href="home.php">Amarprosno.com</a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="home.php">Questions</a></li>
                                    <li><a href="ask.php">Ask Question</a></li>
                                    <li><a href="index.php">Index</a></li>
                                    <li>
                                        <form action="search.php" method="post">
                                        <div class="form-group">
                                            <input type="search" name="search" tabindex="1"
                                                   class="form-control" placeholder="Search Question">
                                        </div></li>
                                    <li><div class="form-group">
                                            <input type="submit" name="btn" class="form-control" placeholder="Search" value="Search">
                                        </div></li>
                                    </form>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">

                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                           aria-haspopup="true" aria-expanded="false">
                                            <span class="glyphicon glyphicon-user"></span>&nbsp;Hi <?php echo  $_SESSION['user']?>&nbsp;<span
                                                    class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="Views/Auth/logout.php"><span></span>Sign Out</a>
                                            <li><a href="profile.php"><span></span>Profile</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!--/.nav-collapse -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>

</div>

<?php

if (isset($_SESSION['message'])){
    echo  $_SESSION['message'];
    unset( $_SESSION['message']);
}
?>
<div class="questions_area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="questions">
                    <h3>All Questions</h3>
                    <hr>
                    <div class="all_question">
                        <?php
                        include "vendor/autoload.php";
                        use App\Auth\Auth;
                        $obj=new Auth();
                        $getallData=$obj->getallQuestions(); ?>
                        <div class="question_list col-md-10">
                            <?php foreach ($getallData as $data){?>
                                <p><a href="questions.php?id=<?php echo $data['question_id']?>"><?php echo $data['title']?></a></p>
                                <span><?php echo substr($data['questions'],0,200)?></span>
                                <p>Posted:<?php echo $data['date_time']?></p>
                            <?php }?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>












    <div class="mainmenu_area">
        <div class="container mainmenu">
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>

    </div>

    <div class="promotions_area">
        <div class="container promotions">
            <div class="row"></div>
        </div>

    </div>
    <div class="projects_area">
        <div class="container projects">
            <div class="row"></div>
        </div>

    </div>
    <div class="featured_projects_area">
        <div class="container featured_projects">
            <div class="row"></div>
        </div>

    </div>
    <div class="footer_top_area">
        <div class="container footer_top">
            <div class="row"></div>
        </div>

    </div>
    <div class="footer_area">
        <div class="container footer">
            <div class="row"></div>
        </div>

    </div>



    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>


</body>
</html>
