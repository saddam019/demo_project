<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>jQuery validation plug-in - main demo</title>
    <link rel="stylesheet" href="css/screen.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="lib/jquery.js"></script>
    <script src="dist/jquery.validate.js"></script>
    <script>
        $().ready(function() {
            // validate the comment form when it is submitted
            $("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    fname: "required",
                    lname: "required",
                    mobile_no: {
                        required: true,
                        minlength: 11
                    },
                    password: {
                        required: true,
                        minlength: 5
                    },
                    username: {
                        required: true,
                        minlength: 5
                    },
                    dob: {
                        required: true,
                        minlength: 5
                    },
                    confirm_password: {
                        required: true,
                        minlength: 5,
                        equalTo: "#password"
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    topic: {
                        required: "#newsletter:checked",
                        minlength: 2
                    },
                    agree: "required"
                },
                messages: {
                    firstname: "Please enter your firstname",
                    lastname: "Please enter your lastname",
                    username: {
                        required: "Please enter a username",
                        minlength: "Your username must consist of at least 2 characters"
                    },
                    password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    confirm_password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long",
                        equalTo: "Please enter the same password as above"
                    },
                    email: "Please enter a valid email address",
                    agree: "Please accept our policy",
                    topic: "Please select at least 2 topics"
                }
            });

            // propose username by combining first- and lastname

        });
    </script>
</head>
<body background="images/bcec8da4.png">
<div class="col-md-12">
    <div class="edit_my_profile">
        <!--        <form action="Views/Auth/add_profile.php" id="signupForm" method="post">
                    <div class="form-group">
                        <label for="title">First Name</label>
                        <input type="text" class="form-control" name="fname" placeholder="First Name">
                    </div>


                    <div class="form-group">
                        <label for="title">Last Name</label>
                        <input type="text" class="form-control" name="lname" placeholder="Last Name">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Mobile No</label>
                        <input type="number" class="form-control" name="mobile_no"
                               placeholder="Mobile No">
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="gender" id="optionsRadios1" value="male" checked>
                            Male
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="gender" id="optionsRadios1" value="female">
                            Female
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Date of Birth</label>
                        <input type="date" class="form-control" name="dob" placeholder="Date of birth">
                    </div>

                    <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" rows="5" name="address" id="comment"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Experience</label>
                        <input type="text" class="form-control" name="experience"
                               placeholder="Experience">
                    </div>

                    <div class="form-group">
                        <input type="submit" name="register-submit" id="register-submit"
                               tabindex="4" class="form-control btn btn-register"
                               value="Submit">
                    </div>
                </form>-->
    </div>
</div>
<div class="login_area">
    <div class="login_head">
        <h1>amarprosno.com</h1>
        <p>Well structured question and answer system</p>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="index.php" >Login</a>
                            </div>
                            <div class="col-xs-6">
                                <a href="#" class="active" >Register</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="signupForm"  action="Views/Auth/store.php"  method="post"
                                      role="form">
                                    <div class="form-group">
                                        <input type="text" name="username" id="username" tabindex="1"
                                               class="form-control" placeholder="Username" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" tabindex="1" class="form-control"
                                               placeholder="Email Address" value="">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" tabindex="2"
                                               class="form-control" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="confirm_password" id="confirm_password"
                                               tabindex="2" class="form-control" placeholder="Confirm Password">
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <input type="submit" name="register-submit" id="register-submit"
                                                       tabindex="4" class="form-control btn btn-register"
                                                       value="Register Now">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





</body>
</html>
